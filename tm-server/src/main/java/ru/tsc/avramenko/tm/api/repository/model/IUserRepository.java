package ru.tsc.avramenko.tm.api.repository.model;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findById(final String id);

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeById(final String id);

    void removeUserByLogin(final String login);

}