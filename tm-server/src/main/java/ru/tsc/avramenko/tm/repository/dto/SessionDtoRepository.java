package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.dto.ISessionDtoRepository;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDtoRepository extends AbstractDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class).getResultList();
    }

    @Override
    public List<SessionDTO> findAllById(String userId) {
        List<SessionDTO> list = entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.user_id = :user_id", SessionDTO.class)
                .setParameter("user_id", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list;
    }

    public SessionDTO findById(@Nullable final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO e")
                .executeUpdate();
    }

    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDTO e WHERE e.userId = :user_id")
                .setParameter("user_Id", userId)
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void remove(SessionDTO session) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, session.getId());
        entityManager.remove(reference);
    }

}