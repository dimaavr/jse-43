package ru.tsc.avramenko.tm.api.repository.dto;

import ru.tsc.avramenko.tm.api.IRepositoryDto;
import ru.tsc.avramenko.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionDtoRepository extends IRepositoryDto<SessionDTO> {

    void remove(final SessionDTO session);

    SessionDTO findById(final String id);

    List<SessionDTO> findAllById(final String userId);

    void clear(final String userId);

}
