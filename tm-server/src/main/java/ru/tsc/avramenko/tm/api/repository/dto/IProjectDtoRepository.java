package ru.tsc.avramenko.tm.api.repository.dto;

import ru.tsc.avramenko.tm.api.IRepositoryDto;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectDtoRepository extends IRepositoryDto<ProjectDTO> {

    ProjectDTO findById(final String userId, final String id);

    ProjectDTO findById(final String id);

    ProjectDTO findByName(final String userId, final String name);

    ProjectDTO findByIndex(final String userId, final int index);

    void removeById(final String userId, final String id);

    void removeById(final String id);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    List<ProjectDTO> findAllById(final String userId);

    void clear(final String userId);

}
