package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.avramenko.tm.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", UserDTO.class).getResultList();
    }

    public UserDTO findById(@Nullable final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDTO e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        UserDTO reference = entityManager.getReference(UserDTO.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        List<UserDTO> list = entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        List<UserDTO> list = entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserDTO e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

}