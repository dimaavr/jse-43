package ru.tsc.avramenko.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class AbstractOwnerEntityDTO extends AbstractEntityDTO {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

}
