package ru.tsc.avramenko.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.ISessionRepository;
import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.api.repository.IUserRepository;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = factory();
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {    @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new ProcessException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new ProcessException();
        @Nullable final String password = propertyService.getJdbcPass();
        if (password == null) throw new ProcessException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new ProcessException();
        @Nullable final String dialect = propertyService.getHibernateDialect();
        if (dialect == null) throw new ProcessException();
        @Nullable final String auto = propertyService.getHibernateHbm2ddl();
        if (auto == null) throw new ProcessException();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        if (sqlShow == null) throw new ProcessException();

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);

        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}