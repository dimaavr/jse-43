package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.dto.AbstractEntityDTO;
import ru.tsc.avramenko.tm.model.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {
}