package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.api.service.dto.*;
import ru.tsc.avramenko.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IDataService getDataService();

    @NotNull
    ITaskDtoService getTaskDtoService();

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IProjectTaskDtoService getProjectTaskDtoService();

    @NotNull
    IUserDtoService getUserDtoService();

    @NotNull
    ISessionDtoService getSessionDtoService();




}