package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.model.ISessionRepository;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.api.service.model.ISessionService;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.repository.model.SessionRepository;
import ru.tsc.avramenko.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.connectionService = connectionService;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        @Nullable final String hash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (hash == null) return false;
        return hash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(serviceLocator.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }


    @Override
    @SneakyThrows
    public void close(@NotNull Session session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            validate(session);
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Session open(@Nullable String login, @Nullable String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            @Nullable final Session resultSession = sign(session);
            repository.add(session);
            entityManager.getTransaction().commit();
            return resultSession;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull Session session, @Nullable Role role) throws AccessDeniedException{
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final User user = session.getUser();
        @Nullable final String userId = user.getId();
        @Nullable final User userExists = serviceLocator.getUserService().findById(userId);
        if (userExists == null) throw new AccessDeniedException();
        if (userExists.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(userExists.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session findById(@Nullable String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

}