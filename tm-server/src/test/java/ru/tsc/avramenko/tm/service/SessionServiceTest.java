package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.service.dto.SessionDtoService;
import ru.tsc.avramenko.tm.service.model.SessionService;

import java.util.List;

public class SessionServiceTest {

    @Nullable
    private SessionDtoService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private SessionDTO session;

    @NotNull
    protected static final String TEST_USER_LOGIN = "TestLogin";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "TestUserPassword";

    @NotNull
    protected static final String TEST_USER_PASSWORD_INCORRECT = "TestUserPasswordIncorrect";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "647";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionDtoService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());

        @Nullable final SessionDTO sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    public void findById() {
        @Nullable final SessionDTO session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final SessionDTO session = sessionService.findById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test
    public void findAllByUserId() {
        @Nullable final List<SessionDTO> session = sessionService.findAll();
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @Nullable final List<SessionDTO> session = sessionService.findAll();
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD_INCORRECT);
        Assert.assertNotNull(session);
    }

    @Test
    public void validate() {
        sessionService.validate(session);
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @Nullable final SessionDTO session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        session.setSignature("647");
        sessionService.validate(session);
    }

}