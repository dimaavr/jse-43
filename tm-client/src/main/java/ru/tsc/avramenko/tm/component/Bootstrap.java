package ru.tsc.avramenko.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.*;
import ru.tsc.avramenko.tm.api.service.*;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.command.system.*;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.exception.system.UnknownCommandException;
import ru.tsc.avramenko.tm.repository.*;
import ru.tsc.avramenko.tm.service.*;
import ru.tsc.avramenko.tm.util.TerminalUtil;
import ru.tsc.avramenko.tm.util.SystemUtil;
import org.reflections.Reflections;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = adminDataEndpointService.getAdminDataEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this, propertyService);

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    public void start(String[] args) {
        displayWelcome();
        initCommands();
        initPID();
        parseArgs(args);
        process();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.avramenko.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.avramenko.tm.command.AbstractCommand.class)
                .stream()
                .sorted((o1, o2) -> o1.getPackage().getName().compareTo(o2.getPackage().getName()))
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    @SneakyThrows
    private void initPID() {
            @NotNull final String filename = "task-manager.pid";
            @NotNull final String pid = Long.toString(SystemUtil.getPID());
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **"+"\n");
    }

    private void commandCompleted(@Nullable final String command) {
        if (System.console() == null) {
            System.out.println("\n" + TerminalUtil.ANSI_GREEN + "Command '" + command + "' completed." + TerminalUtil.ANSI_RESET + "\n");
        }
        else {
            System.out.println("\n" + "Command '" + command + "' completed." + "\n");
        }
    }

    private void displayAuth() {
        if (System.console() == null) {
            System.out.println(TerminalUtil.ANSI_RED + TerminalUtil.AUTHORIZED + TerminalUtil.ANSI_RESET);
        }
        else {
            System.out.println(TerminalUtil.AUTHORIZED);
        }
        System.out.print("\n");
        System.out.println(commandService.getCommandByName("login"));
        System.out.println(commandService.getCommandByName("exit"));
        System.out.println(commandService.getCommandByName("user-create"));
        System.out.println(commandService.getCommandByName("about"));
        System.out.println(commandService.getCommandByName("version"));
        System.out.print("\n");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitCommand().name();
        @NotNull
        String command = "";
        fileScanner.init();
        while (!exit.equals(command)) {
            try {
                while (sessionService.getSession() == null) { //доступные команды для неавторизированных пользователей
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit": parseCommand("exit"); break;
                            case "login": parseCommand("login"); break;
                            case "user-create": parseCommand("user-create"); break;
                            case "about": parseCommand("about"); break;
                            case "version": parseCommand("version"); break;
                            case "": break;
                            default: throw new AccessDeniedException();
                        }
                    } catch (@NotNull final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
                commandCompleted(command);
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
        fileScanner.stop();
    }

    public void parseArg(@Nullable final String arg) {
        @Nullable
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException();
        abstractCommand.execute();
        if (Thread.currentThread().getName().equals("ScannerThread")) {
            commandCompleted(command);
            System.out.println("ENTER COMMAND:");
        }
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}